/** @type {import("snowpack").SnowpackUserConfig } */
const IS_TESTING = process.env.NODE_ENV === 'test';

const mount = {
  public: { url: '/', static: true },
  src: { url: '/dist' },
};

if (IS_TESTING) {
  mount.tests = { url: '/tests' };
}

module.exports = {
  mount,
  plugins: [
    '@snowpack/plugin-postcss',
    '@snowpack/plugin-react-refresh',
    'snowpack-plugin-relative-css-urls',
    '@snowpack/plugin-webpack',
    '@snowpack/plugin-dotenv',
    [
      '@snowpack/plugin-run-script',
      {
        cmd: 'eslint --fix --ext .jsx --ext .js ./',
      },
    ],
  ],
  routes: [
    /* Enable an SPA Fallback in development: */
    { match: 'routes', src: '.*', dest: '/index.html' },
  ],
  optimize: {
    // bundle: true,
    minify: true,
  },
  packageOptions: {
    polyfillNode: IS_TESTING,
  },
  devOptions: {
    tailwindConfig: './tailwind.config.js',
    open: 'false',
  },
  buildOptions: {
    /* ... */
  },
};
