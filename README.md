# Frontend for Easy Collab in ReactJS

## Translation

Currently, this app supports `Hindi` and `English` languages.

The translation uses the `i18n` library.

## API-Docs

https://api.realdev.in/api-docs/

# Usage with VSCode

## Before running the app make sure you install all the packages by running yarn

First, Open up `git bash` (if you're on Windows) or `command prompt` a.k.a `terminal` (on Mac & UNIX systems) and run this command to install packages

```yarn```

After the installation, install recommended visual studio code packages

Here's the list of the extensions

| Recommended extensions |
| ----------- |
| ESLint      |
| Prettier   |
| ES7 React/Redux/GraphQL/React-Native snippets |
| Git Lens   |

Now after installation of these extensions, you can modify the code.

## To start the application, run the following command.
```yarn start```



### Libraries

| Used for   | Links |
| ----------- | ----------- |
| Javascript Bundler      | [Snowpack](https://www.snowpack.dev/tutorials/quick-start)       |
| State Management   | [zustand](https://github.com/pmndrs/zustand)        |
| HTTP Requests   | [ky](https://github.com/sindresorhus/ky)        |
| Managing HTTP Requests Response   | [React Query](https://react-query.tanstack.com/quick-start)    |
| Eslint Configuration | [Airbnb Eslint Config](https://github.com/airbnb/javascript) |
| Form Validation | [React Hook Form](https://react-hook-form.com/get-started/) |
| Testing with CHAI | [Chai.JS](https://www.chaijs.com/api/) |
| Testing with testing-react library | [React testing library](https://testing-library.com/docs/react-testing-library/intro) |
| Multiple Language support | [i18n](https://github.com/mashpie/i18n-node) |
| Styling Library | [Tailwind CSS](https://tailwindcss.com/docs/installation) |
| Icons Library | [Hero Icons](https://github.com/tailwindlabs/heroicons) |
| Patterns Library | [Hero Patterns](https://www.heropatterns.com/) |