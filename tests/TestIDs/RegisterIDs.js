const getRegisterIds = (lang) => ({
  cases: [
    { des: 'check fullname label component', testid: 'fullname' },
    {
      des: 'checking fullname input element',
      testid: 'inputFullName',
    },
    { des: 'checking email label element', testid: 'email' },
    { des: 'checking email input element', testid: 'inputEmail' },
    { des: 'checking password label element', testid: 'password' },
    {
      des: 'checking password input element',
      testid: 'inputPassword',
    },
    {
      des: 'checking confirmPassword label element',
      testid: 'confirmPassword',
    },
    {
      des: 'checking confirmPassword input element',
      testid: 'inputConfirmPassword',
    },
    {
      des: 'checking remember-me checkbox element',
      testid: 'remember-checkbox',
    },
    { des: 'checking remember me link', testid: 'remember-link' },
    { des: 'checking register button', testid: 'register_button' },
    {
      des: 'checking github register button',
      testid: 'github_register_button',
    },
    { des: 'checking login now link', testid: 'login_now_link' },
  ],

  requiredInputValidationCases: [
    {
      des: 'should have full name required validation',
      buttonId: 'register_button',
      validationText: lang.form_errors.enter_full_name,
    },
    {
      des: 'should have password required validation',
      buttonId: 'register_button',
      validationText: lang.form_errors.password_not_found,
    },
    {
      des: 'should have Email required validation',
      buttonId: 'register_button',
      validationText: lang.form_errors.email_required,
    },
    {
      des: 'should have ConfirmPassword required validation',
      buttonId: 'register_button',
      validationText: lang.form_errors.password_confirmation,
    },
  ],

  ValidateUserInput: [
    {
      des: 'fullname should have atleast 4 letters',
      buttonId: 'register_button',
      inputID: 'inputFullName',
      inputText: 'tes',
      validationText: lang.form_errors.name_atleast_four_chars,
    },
    {
      des: 'fullname should not exceed 20 letters',
      buttonId: 'register_button',
      inputID: 'inputFullName',
      inputText: 'AFreakingLongPasswordWhichContainsMoreThan20Chars',
      validationText: lang.form_errors.name_max_twenty_chars,
    },
    {
      des: 'password should have 8 letters atleast',
      buttonId: 'register_button',
      inputID: 'inputPassword',
      inputText: 'test',
      validationText: lang.form_errors.password_min_eight_chars,
    },
    {
      des: 'should have a valid email',
      buttonId: 'register_button',
      inputID: 'inputEmail',
      inputText: 'test132',
      validationText: lang.form_errors.valid_email_id,
    },
  ],
});

export default getRegisterIds;
