const getForgotPasswordIDs = (lang) => ({
  cases: [
    { des: 'checking email label element', testid: 'email' },
    { des: 'checking email input element', testid: 'inputemail' },
    {
      des: 'checking resetpassword button',
      testid: 'forgotPassword_button',
    },
  ],
  requiredInputValidationCases: [
    {
      des: 'should have Email required validation',
      buttonId: 'forgotPassword_button',
      validationText: lang.form_errors.email_required,
    },
  ],
  ValidateUserInput: [
    {
      des: 'should have a valid email',
      buttonId: 'forgotPassword_button',
      inputID: 'inputemail',
      inputText: 'test132',
      validationText: lang.form_errors.valid_email_id,
    },
  ],
  successPage: {
    des: 'should return success page',
    buttonId: 'forgotPassword_button',
    inputID: 'inputemail',
    inputText: 'vector2912@gmail.com',
    validationText: lang.success.forgot_password,
  },
});

export default getForgotPasswordIDs;
