const getResetPasswordIDs = (lang) => ({
  cases: [
    {
      des: 'checking new password label element',
      testid: 'newpassword',
    },
    {
      des: 'checking new password input element',
      testid: 'inputnewpassword',
    },
    {
      des: 'checking verify password label element',
      testid: 'verifypassword',
    },
    {
      des: 'checking verifypassword input element',
      testid: 'inputverifypassword',
    },
  ],

  requiredInputValidationCases: [
    {
      des: 'should have verifypassword required validation',
      buttonId: 'resetpasswordbutton',
      validationText: lang.form_errors.password_confirmation,
    },
  ],

  ValidateUserInput: [
    {
      des: 'password should have 8 letters atleast',
      buttonId: 'resetpasswordbutton',
      inputID: 'inputnewpassword',
      inputText: 'test132',
      validationText: lang.form_errors.password_min_eight_chars,
    },
  ],
  successPage: {
    des: 'password should have 8 letters atleast',
    buttonId: 'resetpasswordbutton',
    inputID: 'inputnewpassword',
    inputText: 'test132',
    validationText: lang.form_errors.password_min_eight_chars,
  },
});

export default getResetPasswordIDs;
