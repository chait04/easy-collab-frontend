import React from 'react';
import { expect } from 'chai';
import { render, screen } from '@testing-library/react';

import '../src/internationalization/index';
import App from '../src/components/App';

describe('App', () => {
  it('renders App component', () => {
    render(<App />);
    const unauth = screen.getByText('Unauthorized Access!!');
    expect(document.body.contains(unauth));
  });
});
