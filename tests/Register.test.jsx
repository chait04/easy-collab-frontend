import React, { useEffect } from 'react';

/* Testing library */
import {
  render,
  screen,
  waitFor,
  fireEvent,
} from '@testing-library/react';
import { expect } from 'chai';

/* additional libraries */

import { QueryClientProvider, QueryClient } from 'react-query';
import { useTranslation } from 'react-i18next';
import { string } from 'prop-types';
import { worker } from '../src/mocks/browser';
import 'antd/dist/antd.css';

import '../src/internationalization/index';
import Register from '../src/components/Authentication/Register';

import {
  testIds,
  inputRequiredValidationIds,
  userInputValidationIds,
} from './testComponents';
import getRegisterIds from './TestIDs/RegisterIDs';
import resources from '../src/internationalization/local-manager';

const RegisterTest = ({ lang }) => {
  // only extracting 'i18n'. Traditional way is "[t, i18n] = ..."
  const i18n = useTranslation()[1];

  const queryClient = new QueryClient();

  useEffect(() => {
    i18n.changeLanguage(lang);
  }, [i18n, lang]);

  return (
    <QueryClientProvider client={queryClient}>
      <Register />
    </QueryClientProvider>
  );
};

RegisterTest.propTypes = {
  lang: string.isRequired,
};

before(() => {
  worker.start({ quiet: true });
});

after(() => worker.stop());

describe('Register', () => {
  const langTest = (currentLang, local) => {
    // getting language information

    const { ValidateUserInput, cases, requiredInputValidationCases } =
      getRegisterIds(local);

    // checking if component exists in body with 'testIDs' function
    testIds(cases, () => render(<RegisterTest lang={currentLang} />));

    // testing required input fields
    inputRequiredValidationIds(requiredInputValidationCases, () =>
      render(<RegisterTest lang={currentLang} />),
    );

    // testing every kind of user input
    userInputValidationIds(ValidateUserInput, () =>
      render(<RegisterTest lang={currentLang} />),
    );

    it('should validate if password and confirming passwords are same.', async () => {
      render(<RegisterTest lang={currentLang} />);

      // selecting targeted elements
      const submitButton = screen.getByTestId('register_button');
      const passwordInput = screen.getByTestId('inputPassword');
      const confirmPassword = screen.getByTestId(
        'inputConfirmPassword',
      );
      // sending events
      fireEvent.change(passwordInput, {
        target: { value: 'somePassword' },
      });
      fireEvent.change(confirmPassword, {
        target: { value: 'wrongPassword' },
      });
      fireEvent.click(submitButton);

      // checking test
      const confirmPasswordError = await waitFor(() =>
        screen.getByText(local.errors.passwords_do_not_match),
      );

      expect(document.body.contains(confirmPasswordError));
    });
  };

  const languages = Object.keys(resources);
  languages.forEach((lang) => {
    langTest(lang, resources[lang].common);
  });
});
