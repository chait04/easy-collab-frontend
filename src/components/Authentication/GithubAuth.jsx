import { func } from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { withTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import { signInWithGithub } from '../../api/auth';

const GithubAuth = ({ t }) => {
  const [isSignedIn, setIsSignedIn] = useState(false);
  const location = useLocation();

  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    const code = searchParams.get('code');
    (async () => {
      if (code) {
        try {
          await signInWithGithub(code);
          setIsSignedIn(true);
        } catch (err) {
          // eslint-disable-next-line no-console
          console.error(err);
        }
      }
    })();
  }, [location]);
  return (
    <div>
      <Helmet>
        <title>
          {t('github_authentication')} • {t('easy_collab')}
        </title>
      </Helmet>
      {isSignedIn ? (
        <h1>{t('success.logged_in')}</h1>
      ) : (
        <h1>{t('status.logging_in')}</h1>
      )}
    </div>
  );
};
GithubAuth.propTypes = {
  t: func.isRequired,
};
export default withTranslation()(GithubAuth);
