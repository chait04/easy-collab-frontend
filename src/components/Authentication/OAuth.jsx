import React from 'react';
import { func } from 'prop-types';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import { Helmet } from 'react-helmet';
import GithubAuth from './GithubAuth';

function OAuth({ t }) {
  const { path } = useRouteMatch();
  // eslint-disable-next-line no-console
  console.log(path);
  return (
    <>
      <Helmet>
        <title>
          {t('authentication')} • {t('easy_collab')}
        </title>
      </Helmet>
      <Switch>
        <Route path={`${path}/github`}>
          <GithubAuth />
        </Route>
      </Switch>
    </>
  );
}
OAuth.propTypes = {
  t: func.isRequired,
};
export default withTranslation()(OAuth);
