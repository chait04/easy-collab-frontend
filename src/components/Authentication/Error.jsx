import React from 'react';
import { withTranslation } from 'react-i18next';
import { func } from 'prop-types';

const Error = ({ t }) => (
  <div>
    <h1>{t('errors.something_went_wrong')}</h1>
  </div>
);

Error.propTypes = {
  t: func.isRequired,
};

export default withTranslation()(Error);
