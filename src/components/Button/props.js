import PropTypes from 'prop-types';

export const LoadingTextProps = {
  propTypes: {
    text: PropTypes.string,
  },
  defaultProps: {
    text: '',
  },
};

const ButtonProps = {
  defaultProps: {
    children: null,
    disabled: false,
    loading: false,
    loadingText: '',
  },
  propTypes: {
    type: PropTypes.string.isRequired,
    variant: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    children: PropTypes.node,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    loadingText: PropTypes.string,
  },
};

export default ButtonProps;
